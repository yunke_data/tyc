﻿# test change

import logging
import os
import re
import threading
import time
from concurrent.futures import ThreadPoolExecutor, process
from lxml import etree
import arrow
import requests
import pandas
from getCompanyNames import getCompanys
#from newGetCompanyNames import getAfterCompanyinit, getAfterCompanys
logging.getLogger("requests").setLevel(logging.WARNING)

logger = logging.getLogger()
logger.setLevel(logging.DEBUG)

ch = logging.StreamHandler()
formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
ch.setFormatter(formatter)
logger.addHandler(ch)


class NewTianYanCha:
    def __init__(self):
        self.base_dir = arrow.now().format("YYYYMMDD-HHmmss")
        self.headers = {
            'user-agent': "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36",
            # 'accept': "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
            # 'accept-encoding': "gzip, deflate, sdch",
            # 'accept-language': "zh-CN,zh;q=0.8,en-US;q=0.6,en;q=0.4",
            # 'cache-control': "no-cache",
        }
        self.lock = threading.RLock()
        self.stats = {"total": 0, "valid": 0}
        self.worker_count = 100
        self.title = "天眼查-人人都在用企业信息调查工具_企业信息查询_公司查询_工商查询_信用查询平台"
        self.ids = self.get_ids()
        self.ids2 = set()



    def get_project(self, job):
        company_name, stats = job
        retry_times = 0
        MAX_RETRY_TIMES = 10
        while retry_times < MAX_RETRY_TIMES:

            proxy_url = "s4.proxy.mayidaili.com:8123"
            try:
                # logging.debug("getting %s, use proxy: %s" % (company_name, proxy_url))
                url = "https://www.tianyancha.com/search?key=%s&checkFrom=searchBox" % company_name
                company_list = requests.request("GET", url, headers=self.headers, timeout=20,verify=False,proxies={"https": proxy_url})
                selector = etree.HTML(company_list.text)
                links = selector.xpath("//div[@class='col-xs-10 search_repadding2 f18']//a//@href")
                if len(links) == 0:
                    break
                for link in links:

                    company_id = re.search(r'[0-9]+', link).group()
                    company_id = int(company_id)
                    if company_id not in self.ids:

                        for retry in range(5):
                            company_info = requests.request("GET", link, headers=self.headers, timeout=20,
                                                            verify=False, proxies={"https": proxy_url})
                            selector = etree.HTML(company_info.text)
                            company_title = selector.xpath("//title/text()")
                            if str(company_title[0]) == self.title:
                                logging.debug("The Download failed try again")
                                continue
                            id = str(company_id)[0:3]
                            out_dir = "/opt2/pagedata0911/%s/" % id
                            if not os.path.exists(out_dir):
                                os.makedirs(out_dir)
                            with self.lock:
                                open("%s/%s.txt" % (out_dir, company_id), "w", encoding="utf-8").write(company_info.text)
                                self.ids.add(company_id)
                                self.ids2.add(company_id)
                                logging.debug("Saving %s" % (company_name))
                            break
                break
            except Exception as e:
                print(e)
            finally:
                retry_times += 1

    def get_ids(self):
        files = os.listdir("/opt2/ids/")
        ids = set()
        for file in files:
            ids |= pandas.read_pickle("/opt2/ids/%s" %file)

        return ids


    def start(self):
        out_dir = "./out"
        if not os.path.exists(out_dir):
            os.makedirs(out_dir)
        self.start = time.time()
        while True:
            executor = ThreadPoolExecutor(max_workers=self.worker_count)
            logging.debug("Loading companys")
            names, offset = getCompanys()
            #names = [x.strip() for x in open("140wan.txt", encoding='utf-8', newline='\n').readlines()]

            print(len(names))
            if len(names) == 0:
                logging.debug("Done all companies")
                break
            names = list(set(names))
            logging.debug("Done load company")
            for name in names:
                executor.submit(self.get_project, (name.replace("\"", ""), self.stats))
            executor.shutdown()

            pandas.to_pickle(self.ids2, "/opt2/ids/%d.pickle" % list(self.ids2)[0])
            self.ids2 = set()

        logging.debug("Done")



tyc = NewTianYanCha()
tyc.start()
