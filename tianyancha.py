# test change
import datetime
import logging
import os
import re
import sys
import threading
import time
import traceback
import ujson as json
from concurrent.futures import ThreadPoolExecutor
from threading import RLock

import arrow
import requests
from chromote import Chromote
from requests.exceptions import ChunkedEncodingError, TooManyRedirects
from requests.packages.urllib3.exceptions import ProtocolError

from getCompanyNames import getCompanyinit, getCompanys
from interfaces import parseSummary
from modules.ProxyProvider import ProxyProvider

logging.getLogger("requests").setLevel(logging.WARNING)

logger = logging.getLogger()
logger.setLevel(logging.INFO)

ch = logging.StreamHandler()
formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s:')
ch.setFormatter(formatter)
logger.addHandler(ch)


class ParseException(BaseException):
    pass


class NoneException(BaseException):
    pass


class BandedException(BaseException):
    pass


class TianYanCha:
    def __init__(self):
        logging.info("setup chrome")
        chrome = Chromote()
        tab = chrome.tabs[0]
        self.tab = tab
        self.tab.set_url("http://m.tianyancha.com/")
        self.browser_lock = RLock()
        self.lock = threading.RLock()
        # self.Proxy_Authorization = "Basic %s" %base64.b64encode(b'HK4O5V0ZIB646H8D:741B64BB03DA1D42').decode()
        self.stats = {"total": 0, "valid": 0}
        self.timeout = 10
        self.base_dir = arrow.now().format("YYYYMMDD-HHmmss")
        self.headers = {
            # 'Proxy-Authorization':self.Proxy_Authorization,
            'user-agent': "Mozilla/5.0 (iPhone; CPU iPhone OS 10_2_1 like Mac OS X) AppleWebKit/602.4.6 (KHTML, like Gecko) Version/10.0 Mobile/14D27 Safari/602.1",
            'accept': "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
            'accept-encoding': "gzip, deflate, sdch",
            'accept-language': "zh-CN,zh;q=0.8,en-US;q=0.6,en;q=0.4",
            'cache-control': "no-cache",
        }

        logging.info("wait browser to be stable")
        self.worker_count = 200
        self.proxy_enabled = True
        if self.proxy_enabled:
            self.proxy_provider = ProxyProvider()
        else:
            self.proxy_provider = None

        time.sleep(5)
        logging.info("done setup chrome")
        logging.info("get_sogou:" + self.get_sogou("2320102897"))

        getCompanyinit()

    def report(self):
        logging.info("report started")
        while True:
            elapsed = time.time() - self.start
            logger.info("Total: %s elasped: %ds, speed: %d, proxycount: %d",
                        self.stats["total"], elapsed, self.stats["total"] / elapsed * 60, self.proxy_provider.count())

            time.sleep(1)

    def get_project(self, job):
        company_name, stats = job
        retry_times = 0
        MAX_RETRY_TIMES = 3
        while retry_times < MAX_RETRY_TIMES:
            proxy_url = None
            if self.proxy_enabled:
                proxy_url = self.proxy_provider.pick()

            try:
                logging.debug("getting %s, use proxy: %s" % (company_name, proxy_url))
                cookies = self.get_init_cookies(company_name, proxy_url)
                security, token = self.get_tokens(cookies, company_name, proxy_url)
                sogou = self.get_sogou(company_name)

                company_list_data = self.get_company_list(cookies, company_name, security, token, sogou, proxy_url)

                company_list = json.loads(company_list_data)

                company_id = company_list["data"][0]["id"]

                # company_id = company_name

                security, token = self.get_tokens(cookies, company_name, proxy_url)
                sogou = self.get_sogou(company_name)
                all = self.get_all(cookies, company_id, security, token, sogou, proxy_url)
                logging.debug(all)
                parseSummary(company_name, company_id, all, proxy_url)

                # self.save(company_id, all)
                stats["total"] += 1
                with self.lock:
                    open("./out/succeed.txt", "a", encoding="utf-8").write(company_name + "\r\n")

                self.proxy_provider.put(proxy_url)
                break
            except (
                    requests.Timeout, requests.ConnectionError, ProtocolError, ChunkedEncodingError,
                    TooManyRedirects) as ex:
                logger.error("Proxy error: %s, excpetion: %s", proxy_url, ex)
            except ParseException:
                self.proxy_provider.put(proxy_url)
                logger.error("parse error %s %s", company_name, proxy_url)
            except BandedException:
                logger.error("Banded error %s %s", company_name, proxy_url)
            except Exception:
                exc_type, exc_value, exc_traceback = sys.exc_info()
                logger.error("%s failed due to %s %s", company_name,
                             traceback.format_exception(exc_type, exc_value, exc_traceback), proxy_url)
            except NoneException:
                self.proxy_provider.put(proxy_url)
                break
            finally:
                retry_times += 1
                if retry_times >= MAX_RETRY_TIMES:
                    with self.lock:
                        open("./out/failure.txt", "a", encoding="utf-8").write(company_name + "\r\n")

    def get_all(self, cookies, company_id, security, token, sogou, proxy):
        url = "http://m.tianyancha.com/expanse/getAll.json?id=%s" % company_id
        logging.debug("get all " + url)

        script = """
        e = function() {

            $SoGou$= %s;

            fxck = '%s'.split(",");
            var fxckStr = "";
            for(var i=0;i<fxck.length;i++){fxckStr+=$SoGou$[fxck[i]];}

            return fxckStr;
        };

        e();
        """ % (sogou, security[0])
        _utm = self.run_script(script)
        cookies['token'] = token[0]
        cookies['_utm'] = _utm
        response = requests.request("GET", url, headers=self.headers, cookies=cookies, timeout=self.timeout,
                                    proxies={"http": proxy})

        self.check_state(url, response, proxy)
        return response.text

    def save(self, company_name, all):
        dir = "./out/%s/%s" % (self.base_dir, company_name)

        if not os.path.exists(dir):
            os.makedirs(dir)

        open(".out/company_all.json", "w", encoding="utf-8").write(all)

    def get_sogou(self, company_name):
        result = self.run_script("testSoGou({'params':{'id':'%s'}}),$SoGou$.join()" % str(company_name))

        return '["' + '","'.join(result.split(',')) + '"]'

    def get_company_list(self, cookies, company_name, security, token, sogou, proxy):
        url = "http://m.tianyancha.com/v2/search/%s.json?" % company_name
        logging.debug(url)

        script = """
                e = function () {

                    $SoGou$= %s;

                    fxck = '%s'.split(",");
                    var fxckStr = "";
                    for(var i=0;i<fxck.length;i++){fxckStr+=$SoGou$[fxck[i]];}

                    return fxckStr;
                };
                e();
                """ % (sogou, security[0])
        _utm = self.run_script(script)
        cookies['token'] = token[0]
        cookies['_utm'] = _utm
        response = requests.request("GET", url, headers=self.headers, cookies=cookies, timeout=self.timeout,
                                    proxies={"http": proxy})
        self.check_state(url, response, proxy)
        return response.text

    def run_script(self, script):
        with self.browser_lock:
            return json.loads(self.tab.evaluate(script))['result']['result']['value']

    def get_tokens(self, cookies, company_name, proxy):
        url = "http://m.tianyancha.com/tongji/%s.json" % company_name
        querystring = {"random": str(int(time.time() * 1000))}

        response = requests.request("GET", url, headers=self.headers, params=querystring, cookies=cookies,
                                    timeout=self.timeout, proxies={"http": proxy})

        self.check_state(url, response, proxy)
        chars = response.json()['data']['v']
        tokens = self.run_script("String.fromCharCode(%s)" % chars)

        token = re.compile(r"""token=(.*?);""").findall(tokens)
        security = re.compile(r"""return'(.*?)'""").findall(tokens)

        return security, token

    def get_init_cookies(self, company_name, proxy):
        url = "http://m.tianyancha.com/search?key=%s" % company_name
        response = requests.request("GET", url, headers=self.headers, timeout=self.timeout, proxies={"http": proxy})
        cookies = response.cookies
        return cookies

    def start(self):
        out_dir = "./out"
        if not os.path.exists(out_dir):
            os.makedirs(out_dir)

        self.start = time.time()

        self.report_thread = threading.Thread(target=self.report)
        self.report_thread.start()

        while True:
            self.start = time.time()
            self.stats = {"total": 0, "valid": 0}
            executor = ThreadPoolExecutor(max_workers=self.worker_count)

            logging.info("Loading companys")
            names, offset = getCompanys()
            if len(names) == 0:
                logging.info("Done all companies")
                return

            names = list(set(names))
            logging.info("Done load company")

            for name in names:
                executor.submit(self.get_project, (name.replace("\"", ""), self.stats))
            executor.shutdown()

            print("Sending")
            elapsed = time.time() - self.start
            speed = self.stats["total"] / elapsed * 60
            resp = self.send_simple_message("TYC " + str(datetime.datetime.today()),
                                            str(self.stats["total"]) + " " + str(speed))

            logging.debug("Done")

    def send_simple_message(self, title, text):
        return requests.post(
            "https://api.mailgun.net/v3/sandbox6b4b82994db247d9891ed7ab4edec7fa.mailgun.org/messages",
            auth=("api", "key-8cd7e31937f1b6e8f3f2c0be5df2bfd8"),
            data={"from": "38288890@qq.com",
                  "to": "38288890@qq.com",
                  "subject": title,
                  "text": text,
                  "html": '<html>%s:</html>' % text})

    def check_state(self, url, response, proxy_url):
        if response.status_code in [501, 403]:
            logging.info("%s %s %s", url, proxy_url, response.text)
            if "squid" in response.text:
                with open("./out/errorip.txt", "a") as f:
                    f.write(proxy_url + "\n")
            raise BandedException()

        try:
            j = json.loads(response.text)
            if j['state'] == 'warn':
                raise NoneException()
            elif j['state'] != 'ok':
                logging.debug(
                    "Checking state failed:%s, %d, %s, %s" % (url, response.status_code, response.text, proxy_url))
                raise ParseException()
        except Exception as ex:
            logging.debug(
                "Checking state failed:%s, %d, %s, %s" % (url, response.status_code, response.text, proxy_url))
            raise ParseException()


tyc = TianYanCha()
tyc.start()
