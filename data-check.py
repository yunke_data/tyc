import glob
import ujson as json

error_count = 0
for dir in glob.glob("/opt/data/out/**"):
    for file in glob.glob(dir + "/*.json", recursive=True):
        has_error = False
        with open(file, "r") as f:
            obj = json.loads(f.read())
        summary = obj['summary']
        for k in summary.keys():
            if k == 'equityUrl':
                continue

            value_in_summary = int(summary[k])
            if value_in_summary != 0:
                error = ""
                if k not in obj:
                    has_error = True
                    print("%s,%s,%s,%s,%s" % (error_count, file, k, value_in_summary, 'missing'))
                else:
                    real_value = obj[k]
                    if real_value is None:
                        has_error = True
                        print("%s,%s,%s,%s" % (error_count, file, k, 'value is none'))

        if has_error:
            error_count += 1
