class Proxy:
    def __init__(self, url):
        self._url = url
        self._used = 1
        self._success = 1
        self._fatal_errors = 1
        self._connection_errors = 1
        self._parse_errors = 1
        pass

    @property
    def url(self):
        return self._url

    def used(self):
        self._used += 1

    def success(self):
        self._success += 1

    def fatal_error(self):
        self._fatal_errors += 1

    def connection_error(self):
        self._connection_errors += 1

    def parse_error(self):
        self._parse_errors += 1

    @property
    def score(self):
        return int(self._success * 1.0 / self._used * 100)

    def get_stats(self):
        return self._used, self._success, self._connection_errors, self._parse_errors, self._fatal_errors
