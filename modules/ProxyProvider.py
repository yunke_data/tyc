import datetime
import logging
import random
import threading
import time
import ujson

import requests

from modules.Proxy import Proxy

logger = logging.getLogger()


class ProxyProvider:
    def __init__(self, num_of_proxies=200):
        self.num_of_proxies = num_of_proxies
        self.lock = threading.RLock()
        self.last_dump = time.time()
        self.get_list()

    def get_list(self):
        with self.lock:
            logger.debug("Getting proxy list")
            r = requests.get("http://proxy.farecool.com:8000/proxy.json", timeout=10)
            proxies = ujson.decode(r.text)
            logger.debug("Got %s proxies", len(proxies))
            self._proxies = list(map(lambda p: Proxy(p), proxies))
            self._last_load = datetime.datetime.now()

    def pick(self):
        pass

    def count(self):
        with self.lock:
            self.dump()
            return len(self._proxies)

    def dump(self):
        with self.lock:
            if time.time() - self.last_dump > 2:
                self.last_dump = time.time()
                with open("./out/proxies.txt", "w", encoding="utf-8") as f:
                    f.write("Total proxies: %d\n" % len(self._proxies))
                    f.write("url\t\t\t\tscore\tused\tsuccess\tconn\tparse\tfatal\n")
                    for proxy in self._proxies:
                        used, success, connection_errors, parser_errors, fatal_errors = proxy.get_stats()
                        f.write("{0:30}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\n".format(proxy.url, proxy.score, used, success,
                                                                                connection_errors, parser_errors,
                                                                                fatal_errors))

    def refresh(self):
        with self.lock:
            if (datetime.datetime.now() - self._last_load).total_seconds() > 60 * 10:
                self.get_list()


class RandomScoreSortedProxyProvider(ProxyProvider):
    def pick(self):
        with self.lock:
            self._proxies.sort(key=lambda p: p.score, reverse=True)
            proxy_len = len(self._proxies)
            max_range = self.num_of_proxies if proxy_len > self.num_of_proxies else proxy_len
            proxy = self._proxies[random.randrange(1, max_range)]
            proxy.used()

            return proxy


class SequenceProxyProvider(ProxyProvider):
    def __init__(self, num_of_proxies=200):
        super().__init__(num_of_proxies)
        self.index = 0

    def pick(self):
        with self.lock:
            self._proxies.sort(key=lambda p: p.score, reverse=True)
            proxy = self._proxies[self.index]
            proxy.used()

            proxy_len = len(self._proxies)
            max = self.num_of_proxies if proxy_len > self.num_of_proxies else proxy_len
            self.index = (self.index + 1) % max
            return proxy


if __name__ == "__main__":
    provider = RandomScoreSortedProxyProvider()
    print(provider.pick().url)

    provider = SequenceProxyProvider()
    print(provider.pick().url)
