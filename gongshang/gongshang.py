import datetime
import hashlib
import re
import time
from concurrent.futures import ThreadPoolExecutor
from threading import RLock

import requests
from requests.packages.urllib3.exceptions import InsecureRequestWarning

requests.packages.urllib3.disable_warnings(InsecureRequestWarning)

import constants
import logging

logger = logging.getLogger()
logger.setLevel(logging.DEBUG)

ch = logging.StreamHandler()
formatter = logging.Formatter('%(threadName)s - %(asctime)s - %(levelname)s - %(message)s:')
ch.setFormatter(formatter)
logger.addHandler(ch)

fh = logging.FileHandler('./out/out.log')
fh.setLevel(logging.DEBUG)
fh.setFormatter(formatter)
logger.addHandler(fh)

import pymysql.cursors

MAX_RETRY_TIMES = 10


class MissingFinder:
    def get_auth_token(self):
        my_app_key = "111877882"
        app_secret = "1451452bef315cb2da8b09ef0cba221a"
        mayi_url = 's4.proxy.mayidaili.com'
        mayi_port = '8123'
        mayi_proxy = {'https': 'https://{}:{}'.format(mayi_url, mayi_port)}

        timesp = '{}'.format(time.strftime("%Y-%m-%d %H:%M:%S"))
        codes = app_secret + 'app_key' + my_app_key + 'timestamp' + timesp + app_secret
        sign = hashlib.md5(codes.encode('utf-8')).hexdigest().upper()

        authHeader = 'MYH-AUTH-MD5 sign=' + sign + '&app_key=' + my_app_key + '&timestamp=' + timesp

        s = requests.Session()
        s.headers.update({
            'user-agent': "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36"})
        s.headers.update({'Proxy-Authorization': authHeader})
        s.proxies.update(mayi_proxy)

        return s

    def start(self):
        self.lock = RLock()
        self.output_file = open("./out/companies.csv", "w")
        self.regex_company_count = re.compile('(\d+)</span>\s+<span.*>家相关公司</span>')
        self.regex_pages_count = re.compile('<span>共</span>\s*(\d+)\s*<span>')
        self.regex_company_names = re.compile('search_left_icon\">\s*<img alt=\"(.*?)\".*?<a\shref="(.*?)"',
                                              flags=re.DOTALL)

        self.regex_province = re.compile('3px;"></i>(.*?)\s+<div', flags=re.DOTALL)
        self.regex_status = re.compile('statusTypeNor statusType.*?>\s*(.*?)\s*</div>', flags=re.DOTALL)
        self.regex_person = re.compile('法定代表人.*?>(.*?)</span>', flags=re.DOTALL)
        self.regex_total_money = re.compile('注册资本：<span\s*title="(.*?)"', flags=re.DOTALL)
        self.regex_reg_date = re.compile('注册时间：<span\s*title="(.*?)"', flags=re.DOTALL)

        self.connection = pymysql.connect(host='rds0710650me01y6d3ogo.mysql.rds.aliyuncs.com',
                                          user='yunkedata',
                                          password='XingDongJia@2016',
                                          db='dataprocess',
                                          charset='utf8',
                                          cursorclass=pymysql.cursors.DictCursor)
        self.db_lock = RLock()
        self.begin_num = self.get_total_number_of_names()
        self.start = datetime.datetime.now()
        self.total_in_db = self.begin_num
        self.total_valid = 0
        self.last_speed_refresh_time = datetime.datetime.now()

        executor = ThreadPoolExecutor(max_workers=50)
        category = [
            1, 2, 3, 4, 5,  # 农、林、牧、渔业
            63, 64, 65,  # 信息传输、软件和信息技术服务业
            79, 80, 81,  # 居民服务、修理和其他服务业
            83, 84,  # 医美行业，医疗行业
            82,  # 教育
            73, 74, 75,  # 科学研究和技术服务业
            47, 48, 49, 50,  # 建筑业
            13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43,  # 制造业
            96,  # 国际组织
            90, 91, 92, 93, 94, 95,  # 公共管理、社会保障和社会组织
            51, 52,  # 批发零售
            71, 72,  # 租赁和商务服务业
            61, 62,  # 住宿和餐饮业
            66, 67, 68, 69,  # 金融业
            70,  # 房地产业
            85, 86, 87, 88, 89,  # 文化、体育和娱乐业
            53, 54, 55, 56, 57, 58, 59, 60,  # 交通运输、仓储和邮政业
            76, 77, 78,  # 水利、环境和公共设施管理业
            44, 45, 46,  # 电力、热力、燃气及水生产和供应业
            6, 7, 8, 9, 10, 11, 12,  # 采矿业
        ]
        for i in category:
            for domain in constants.domains:
                cityname = domain[1]
                for key in [cityname, '省', '县']:
                    executor.submit(self.get_company_count, (domain[0], i, key))

        executor.shutdown(wait=True)

        self.output_file.close()

    def build_url(self, domain, i, key, e, r, s, page):
        url = domain + "/search/oc%02d" % i

        if r:
            url += "-r%s" % r

        if e:
            url += "-e%s" % e

        if s:
            url += "-s%s" % s

        url += "-la3"
        if page:
            url += "/p%d" % page

        url += "?key=%s&searchType=company" % key

        logging.info("Getting url %s", url)
        return url

    def is_url_searched(self, url):
        with self.db_lock:
            with self.connection.cursor() as cursor:
                # Read a single record
                sql = "SELECT * FROM searched_urls WHERE url='%s'" % (url)
                cursor.execute(sql)
                result = cursor.fetchone()
                return result is not None

    def get_total_number_of_names(self):
        with self.db_lock:
            with self.connection.cursor() as cursor:
                # Read a single record
                sql = "SELECT count(*) AS cnt FROM company_names"
                cursor.execute(sql)
                result = cursor.fetchone()
                return result['cnt']

    def get_more_companies(self, domain, i, key, e, r, s):
        url = self.build_url(domain, i, key, e, r, s, None)

        company_count = 0
        pages_count = 0
        retry_times = 0
        while retry_times < MAX_RETRY_TIMES:
            try:
                logger.debug("Getting %s, retry_times %s", url, retry_times)
                retry_times += 1
                token = self.get_auth_token()

                if self.is_url_searched(url):
                    logger.debug("url searched %s", url)
                    break

                resp = token.get(url, timeout=5, verify=False, allow_redirects=False)
                if resp.status_code != 200:
                    logger.debug("Retry %s, %s", url, resp.status_code)
                    continue

                search_result = resp.text
                company_count_matches = self.regex_company_count.findall(search_result)
                if len(company_count_matches) == 0:
                    break

                company_count = int(company_count_matches[0])
                if company_count == 0:
                    break

                pages_count = self.get_page_count(company_count, search_result)

                logger.debug("Company count %s, page_count %s", company_count, pages_count)
                self.get_company_for_page(domain, i, key, e, r, s, pages_count)
                break
            except Exception as ex:
                logger.error(ex)

        self.mark_searched(url, company_count, pages_count)
        return company_count

    def get_company_for_page(self, domain, i, key, e, r, s, page_count):
        if page_count == 0:
            self.get_company_for_single_page(domain, e, i, key, r, s)
        else:
            self.get_company_for_multiple_page(domain, e, i, key, page_count, r, s)

    def get_company_for_multiple_page(self, domain, e, i, key, page_count, r, s):
        for page_index in range(1, page_count):
            retry_times = 0
            while retry_times < MAX_RETRY_TIMES:
                retry_times += 1
                try:
                    url = self.build_url(domain, i, key, e, r, s, page_index)
                    token = self.get_auth_token()

                    if self.is_url_searched(url):
                        logger.debug("url searched %s", url)
                        break

                    resp = token.get(url, timeout=5, verify=False, allow_redirects=False)
                    if resp.status_code != 200:
                        continue

                    self.extract_company_names(resp.text, i)
                    break
                except Exception as ex:
                    logger.error("Retrying %s", ex)

    def get_company_for_single_page(self, domain, e, i, key, r, s):
        retry_times = 0
        while retry_times < MAX_RETRY_TIMES:
            retry_times += 1

            try:
                url = self.build_url(domain, i, key, e, r, s, None)
                token = self.get_auth_token()

                if self.is_url_searched(url):
                    logger.debug("url searched %s", url)
                    break

                resp = token.get(url, timeout=5, verify=False, allow_redirects=False)
                if resp.status_code != 200:
                    continue

                self.extract_company_names(resp.text, i)
                break
            except Exception as ex:
                logger.error("Retrying %s", ex)

    def extract_company_names(self, content, category):
        with self.db_lock:
            try:
                company_names = self.regex_company_names.findall(content)
                statuss = self.regex_status.findall(content)
                provinces = self.regex_province.findall(content)
                reg_dates = self.regex_reg_date.findall(content)
                persons = self.regex_person.findall(content)
                total_moneys = self.regex_total_money.findall(content)

                for i in range(0, len(company_names)):
                    with self.connection.cursor() as cursor:
                        sql = """INSERT IGNORE company_names (name, URL, category, status, province, reg_date, person, money) VALUES ('%s','%s', '%s','%s','%s','%s', '%s', '%s')""" % (
                            company_names[i][0], company_names[i][1], category, statuss[i], provinces[i], reg_dates[i], persons[i], total_moneys[i])
                        cursor.execute(sql)
                self.connection.commit()

                duration_minutes = (datetime.datetime.now() - self.start).total_seconds() / 60.0

                if (datetime.datetime.now() - self.last_speed_refresh_time).total_seconds() > 60:
                    self.last_speed_refresh_time = datetime.datetime.now()
                    self.total_in_db = self.get_total_number_of_names()
                    self.total_valid = self.total_in_db - self.begin_num

                logger.info("Inserted %s, total %s, total in db %s, elapsed %s, speed %f", len(company_names), self.total_valid, self.total_in_db, duration_minutes,
                            self.total_valid / duration_minutes)
            except Exception as ex:
                logging.debug(ex)

    def get_company_count(self, job):
        try:
            domain, i, key = job

            url = self.build_url(domain, i, key, None, None, None, None)

            retry_times = 0
            while retry_times < MAX_RETRY_TIMES:
                try:
                    token = self.get_auth_token()
                    logger.debug("Getting %s, %s", url, retry_times)
                    retry_times += 1

                    if self.is_url_searched(url):
                        logger.debug("url searched %s", url)
                        return

                    resp = token.get(url, timeout=5, verify=False, allow_redirects=False)
                    if resp.status_code != 200:
                        continue

                    search_result = resp.text
                    company_count_result = self.regex_company_count.findall(search_result)
                    if len(company_count_result) == 0:
                        self.mark_searched(url, 0, 0)
                        break

                    company_count = int(company_count_result[0])
                    if company_count > 5000:
                        total = 0
                        logger.debug("Company count > 5000 %s", company_count)
                        for e in ['01', '015', '510', '1015', '15']:
                            for r in ['0100', '100200', '200500', '5001000', '1000']:
                                if company_count > 10000:
                                    for s in ['1', '2', '3', '4', '5']:
                                        total += self.get_more_companies(domain, i, key, e, r, s)
                                else:
                                    total += self.get_more_companies(domain, i, key, e, r, None)

                        logger.debug("Company count %s, total %s", company_count, total)
                        self.mark_searched(url, company_count, 0)
                    else:
                        pages_count = self.get_page_count(company_count, search_result)
                        self.get_company_for_page(domain, i, key, None, None, None, pages_count)
                        self.mark_searched(url, company_count, pages_count)
                    break
                except Exception as ex:
                    logger.error(ex)
        except Exception as ex:
            logger.error(ex)

    def get_page_count(self, company_count, search_result):
        if company_count <= 20:
            pages_count = 0
        else:
            pages_count_matches = self.regex_pages_count.findall(search_result)
            pages_count = int(pages_count_matches[0])
        return pages_count

    def mark_searched(self, url, count, pages):
        with self.db_lock:
            with self.connection.cursor() as cursor:
                sql = """INSERT IGNORE searched_urls (url, count, pages) VALUES('%s','%s', '%s')""" % (url, count, pages)
                cursor.execute(sql)
            self.connection.commit()


MissingFinder().start()
