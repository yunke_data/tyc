import time
from chromote import Chromote

chrome = Chromote()
tab = chrome.tabs[0]
e = tab._send({"method": "Network.enable"})
print(e)

time.sleep(1)
e = tab._send({"method": "Network.setUserAgentOverride", "params": {"userAgent": "Mozilla/5.0 (iPhone; CPU iPhone OS 10_2_1 like Mac OS X) AppleWebKit/602.4.6 (KHTML, like Gecko) Version/10.0 Mobile/14D27 Safari/602.1"}})
print(e)

time.sleep(1)
e = tab._send({"method": "Page.enable"})

time.sleep(1)
e = tab._send({"method": "Page.navigate", "params":{
    "url":"http://m.tianyancha.com"
}})