import glob
import os

for dir in glob.glob("/opt/data/out/*"):
    zip_dir = os.path.basename(dir)
    zip_file = "/opt/data/zip/" + zip_dir + ".zip"
    cmd = "zip -ur %s %s" % (zip_file, dir)
    print(cmd)
    os.system(cmd)
