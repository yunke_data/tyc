import os
import pickle
import shutil

import distance
import six
from distutils.dir_util import copy_tree

with open("../out/images.pickle", "rb") as f:
    imgs = pickle.load(f)

images = {}
for k, img_list in six.iteritems(imgs):
    images[str(k)] = img_list

keys = list(images.keys())

merged = []
while len(keys) != 0:
    k = keys.pop()
    dis = []
    for k2 in keys:
        d = distance.hamming(k, k2)
        dis.append({'key': k2, 'value': d})

    filtered = list(map(lambda x: x['key'], list(filter(lambda x: x['value'] < 5, dis))))

    pics = []
    for i in filtered:
        pics += images[i]
        keys.remove(i)

    if len(pics) != 0:
        merged.append(pics)

base_dir = "../out/image_divided/"
shutil.rmtree("%s" % base_dir, ignore_errors=True)
os.mkdir(base_dir)

for i in range(0, len(merged)):
    if len(merged[i]) > 10:
        dir = "%s/%s" % (base_dir, (str(i)))
        os.mkdir(dir)

        for f in merged[i]:
            filename = os.path.basename(f)
            shutil.copy(f, os.path.join(dir, filename))
