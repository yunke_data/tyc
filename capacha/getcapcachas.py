import base64

import requests

for i in range(0, 1000):
    resp = requests.request("GET",
                            "http://antirobot.tianyancha.com/captcha/getCaptcha.json?t=1499066454182&_=1499066454158",
                            proxies={"http": "http://117.143.109.148:80"})

    d = resp.json()['data']

    filename = "../out/images/" + d['id'] + ".png"
    with open(filename, "wb") as f:
        print(filename)
        f.write(base64.b64decode(d['bgImage']))
