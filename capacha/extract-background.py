import glob

import cv2
import numpy as np
from matplotlib import pyplot as plt

last_img = None
for i in glob.glob("../out/image_divided/0/*.png", recursive=True):
    img = cv2.imread(i)
    if last_img is None:
        last_img = img
        continue
    else:
        diff = last_img - img
        diff = np.sum(diff, axis=1)

        plt.imshow(diff)
        pass

plt.imshow(last_img)
