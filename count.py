import json
import logging
import re
import time
import sys
import arrow
import execjs
import requests
from chromote import Chromote

logging.getLogger("requests").setLevel(logging.WARNING)

logger = logging.getLogger()
logger.setLevel(logging.DEBUG)

ch = logging.StreamHandler()
formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s:')
ch.setFormatter(formatter)
logger.addHandler(ch)


class TianYanChaCount:
    def __init__(self):
        logging.debug("setup chrome")
        chrome = Chromote()
        tab = chrome.tabs[0]

        self.tab = tab
        self.tab.set_url("http://www.tianyancha.com/")
        logging.debug("wait")
        time.sleep(5)
        logging.debug("get_sogou:" + self.get_sogou("2320102897"))

        self.stats = {"total": 0, "valid": 0}
        self.timeout = 10
        self.base_dir = arrow.now().format("YYYYMMDD-HHmmss")
        self.headers = {
            'user-agent': "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36",
            'accept': "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
            'accept-encoding': "gzip, deflate, sdch",
            'accept-language': "zh-CN,zh;q=0.8,en-US;q=0.6,en;q=0.4",
            'cache-control': "no-cache",
        }

        logging.debug("done setup chrome")

    def search(self, name):
        cookies = dict()
        security, token, cookies = self.get_tokens(cookies, name, None)

        sogou = self.get_sogou(name)
        content = self.get_company_id(cookies, name, security, token, sogou, None)
        data = json.loads(content)
        print(data)
        logging.info(data)

    def get_sogou(self, project_id):
        result = json.loads(self.tab.evaluate("testSoGou({'params':{'id':'%s'}}),$SoGou$.join()" % str(project_id)))

        return '["' + '","'.join(result['result']['result']['value'].split(',')) + '"]'

    def get_company_id(self, cookies, name, security, token, sogou, proxy):
        url = "http://www.tianyancha.com/expanse/getAll.json?id=" + name
        #print(url)

        script = """
        function() {

            $SoGou$= %s;

            fxck = '%s'.split(",");
            var fxckStr = "";
            for(var i=0;i<fxck.length;i++){fxckStr+=$SoGou$[fxck[i]];}

            return fxckStr;
        }()
        """ % (sogou, security[0])
        _utm = execjs.eval(script)
        cookies['token'] = token[0]
        cookies['_utm'] = _utm
        response = requests.request("GET", url, headers=self.headers, cookies=cookies, timeout=self.timeout,
                                    proxies={"http": proxy})

        self.check_state(response.text)
        return response.text

    def get_tokens(self, cookies, project_id, proxy):
        url = "http://www.tianyancha.com/tongji/%s.json" % project_id
        querystring = {"random": str(int(time.time() * 1000))}

        response = requests.request("GET", url, headers=self.headers, params=querystring, cookies=cookies,
                                    timeout=self.timeout,
                                    proxies={"http": proxy})

        self.check_state(response.text)
        chars = response.json()['data']['v']
        tokens = execjs.eval("String.fromCharCode(%s)" % chars)

        token = re.compile(r"""token=(.*?);""").findall(tokens)
        security = re.compile(r"""return'(.*?)'""").findall(tokens)

        return security, token, response.cookies

    def get_cookies(self, project_id, proxy):
        url = "http://www.tianyancha.com/company/%s" % project_id
        print(url)
        response = requests.request("GET", url, headers=self.headers, timeout=self.timeout, proxies={"http": proxy})
        cookies = response.cookies
        return cookies

    def check_state(self, response):
        j = json.loads(response)
        if j['state'] != 'ok':
            print(response)
            raise Exception("state does not ok")


tyc = TianYanChaCount()
tyc.search(sys.argv[1])