
INTERFACE = {
    "changeCount": "http://m.tianyancha.com/expanse/changeinfo.json?id=%s&ps=100&pn=1",               # 变更记录
    "punishment": "http://m.tianyancha.com/expanse/punishment.json?name=%s&ps=100&pn=1",              # 行政处罚
    "reportCount": "http://m.tianyancha.com/expanse/annu.json?id=%s&ps=100&pn=1",                     # 企业年报
    "zhixing": "http://m.tianyancha.com/expanse/zhixing.json?id=%s&pn=1&ps=100",                      # 被执行人
    "recruitCount": "http://m.tianyancha.com/extend/getEmploymentList.json?companyName=%s&pn=1&ps=100",  # 招聘
    "checkCount": "http://m.tianyancha.com/expanse/companyCheckInfo.json?name=%s&pn=1&ps=100",        # 抽查检查
    "dishonest": "http://m.tianyancha.com/v2/dishonest/%s.json",                                      # 失信人
    "jigouTzanli": "http://m.tianyancha.com/expanse/findTzanli.json?name=%s&ps=100&pn=1",          # 投资事件
    "staffCount": "http://m.tianyancha.com/expanse/staff.json?id=%s&ps=100&pn=1",                     # 主要人员
    "qualification": "http://m.tianyancha.com/expanse/qualification.json?id=%s&ps=100&pn=1",          # 资质证书
    "companyPortray": "http://m.tianyancha.com/v2/company/%s.json",                                   # 基本信息
    "patentCount": "http://m.tianyancha.com/expanse/patent.json?id=%s&pn=1&ps=100",                   # 专利
    "inverstCount": "http://m.tianyancha.com/expanse/inverst.json?id=%s&ps=100&pn=1",                 # 对外投资
    "bidCount": "http://m.tianyancha.com/expanse/bid.json?id=%s&pn=1&ps=100",                         # 招投标
    "tmCount": "http://m.tianyancha.com/tm/getTmList.json?id=%s&pageNum=1&ps=100",                    # 商标信息
    "companyRongzi": "http://m.tianyancha.com/expanse/findHistoryRongzi.json?name=%s&ps=100&pn=1",    # 融资历史
    "taxCreditCount": "http://m.tianyancha.com/expanse/taxcredit.json?id=%s&ps=100&pn=1",       # 税务评级
    "companyJingpin": "http://m.tianyancha.com/expanse/findJingpin.json?name=%s&ps=100&pn=1",         # 竞品信息
    # "ownTaxCount": "0",         # 欠税公告（无接口）
    "branchCount": "http://m.tianyancha.com/expanse/branch.json?id=%s&ps=100&pn=1",                   # 分之机构
    # "illegalCount": "0",        # 严重违法（无接口）
    "holderCount": "http://m.tianyancha.com/expanse/holder.json?id=%s&ps=100&pn=1",                   # 股东信息
    "icpCount": "http://m.tianyancha.com/v2/IcpList/%s.json",                                         # 网站备案
    "lawsuitCount": "http://m.tianyancha.com/v2/getlawsuit/%s.json?page=1&ps=100",                    # 法律诉讼
    "mortgageCount": "http://m.tianyancha.com/expanse/mortgageInfo.json?name=%s&pn=1&ps=100",         # 动产抵押
    "productinfo": "http://m.tianyancha.com/expanse/appbkinfo.json?id=%s&ps=100&pn=1",                # 产品信息
    "abnormalCount": "http://m.tianyancha.com/expanse/abnormal.json?id=%s&ps=100&pn=1",               # 经营异常
    "equityCount": "http://m.tianyancha.com/expanse/companyEquity.json?name=%s&ps=100&pn=1",          # 股权出质
    "courtCount": "http://m.tianyancha.com/v2/court/%s.json?",                                        # 法院公告
    # "goudiCount": "0",      # 购地信息（无接口）
    "cpoyRCount": "http://m.tianyancha.com/expanse/copyReg.json?id=%s&pn=1&ps=100",                   # 著作权
    # "bondCount": "0",       # 债券信息（无接口）
    "companyTeammember": "http://m.tianyancha.com/expanse/findTeamMember.json?name=%s&ps=100&pn=1",   # 核心团队
    "companyProduct": "http://m.tianyancha.com/expanse/findProduct.json?name=%s&ps=100&pn=1"          # 企业业务
}
idSet = set(["changeCount","reportCount","zhixing","staffCount","qualification","companyPortray",
            "patentCount","inverstCount","bidCount","tmCount","taxCreditCount","holderCount",
            "icpCount","productinfo","abnormalCount","cpoyRCount"])
nameSet = set(["punishment","recruitCount","checkCount","dishonest","jigouTzanli","companyRongzi",
            "companyJingpin","branchCount","lawsuitCount","mortgageCount","equityCount","courtCount",
            "companyTeammember","companyProduct"])
lookup = {
    "cid":"ent_id",
    "title":"ent_name",
    "summary":"json_num",
    "changeCount": "a_changeRecord",# 变更记录
    "punishment": "d_administrativeSanction",# 行政处罚
    "reportCount": "a_annualReports",# 企业年报
    "zhixing": "c_beExecutionPerson",# 被执行人
    "recruitCount": "e_recruit",  # 招聘
    "checkCount": "e_spotCheck",# 抽查检查
    "dishonest": "c_dishonestPerson",# 失信人
    "jigouTzanli": "b_investmentEvent",# 投资事件
    "staffCount": "a_main_person",# 主要人员
    "qualification": "e_qualificationCertificate",# 资质证书
    "companyPortray": "a_base_Info",# 基本信息
    "patentCount": "f_patent",# 专利
    "inverstCount": "a_investment",# 对外投资
    "bidCount": "e_bidding",# 招投标
    "tmCount": "f_trademarkInformation",# 商标信息
    "companyRongzi": "b_financingHistory",# 融资历史
    "taxCreditCount": "e_taxRating",# 税务评级
    "companyJingpin": "b_competingInformation",# 竞品信息
    "ownTaxCount": "d_taxNotice",# 欠税公告（无接口）
    "branchCount": "a_branchOrg",# 分之机构
    "illegalCount": "d_seriousViolation",        # 严重违法（无接口）
    "holderCount": "a_shareholder",# 股东信息
    "icpCount": "f_websiteFiling",# 网站备案
    "lawsuitCount": "c_legalProceedings",# 法律诉讼
    "mortgageCount": "d_chattelMortgage",# 动产抵押
    "productinfo": "e_productInformation",# 产品信息
    "abnormalCount": "d_abnormalOperation",# 经营异常
    "equityCount": "d_stockOwnership",# 股权出质
    "courtCount": "c_courtNotice",# 法院公告
    "goudiCount": "e_purchaseInformation",# 购地信息（无接口）
    "cpoyRCount": "f_copyright",# 著作权
    "bondCount": "e_bondInformation",# 债券信息（无接口）
    "companyTeammember": "b_coreTeam",# 核心团队
    "companyProduct": "b_enterpriseBusiness"# 企业业务
}

if __name__ == '__main__':
    for name in nameSet:
        print(name)
        print(INTERFACE[name] % "test")