import json
import subprocess
import platform
import time

if platform.system() == "Darwin":
    exec = "/Applications/Google\ Chrome\ Canary.app/Contents/MacOS/Google\ Chrome\ Canary"
else:
    exec = "google-chrome"


while True:
    with open("./proxy.json") as p:
        new_proxies = json.load(p)

    for proxy in new_proxies:
        print(proxy)
        s = subprocess.Popen(
            exec + ' --disable-gpu --user-data=./userdata --proxy-server="http=%s" --user-agent="Mozilla/5.0 (iPhone; CPU iPhone OS 10_2_1 like Mac OS X) AppleWebKit/602.4.6 (KHTML, like Gecko) Version/10.0 Mobile/14D27 Safari/602.1" http://www.tianyancha.com/search?key=摩拜' % proxy,
            shell=True)

        print("Wait")
        time.sleep(10)
        print("Kill")
        s.kill()
