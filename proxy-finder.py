from concurrent.futures import ThreadPoolExecutor

import requests

good = []

executor = ThreadPoolExecutor(max_workers=30)


def check(ip):
    print(ip)

    resp = requests.get("http://www.tianyancha.com", proxies={"http": ip}, timeout=2)
    if '地址：北京市海淀区知春路63号中国卫星通信大厦B座23层' in resp.text:
        good.append(ip.replace("http://", ""))
        print(good)


for b in range(1, 255):
    base_ip = "180.96.5.%s:8080" % (str(b))
    ip = "http://" + base_ip

    executor.submit(check, ip)

executor.shutdown()
print(good)
