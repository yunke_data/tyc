import os
import ujson as json
import logging
import re
from urllib.parse import unquote,quote

import pymysql
import requests

# from tornado import gen, httpclient, ioloop, queues, web
import threading

from field import *

def connectMySqlWrite():
    return pymysql.connect(host='rm-2ze4w2b7l842282xmo.mysql.rds.aliyuncs.com',
                           user='root',
                           passwd="dsfi89498(&^F&BIs9dif",
                           db="company_",
                           use_unicode=True,
                           charset='utf8',
                           cursorclass=pymysql.cursors.DictCursor)


dbwrite = connectMySqlWrite()


lock = threading.RLock()

headers = {
    # 'Proxy-Authorization':self.Proxy_Authorization,
    'user-agent': "Mozilla/5.0 (iPhone; CPU iPhone OS 10_2_1 like Mac OS X) AppleWebKit/602.4.6 (KHTML, like Gecko) Version/10.0 Mobile/14D27 Safari/602.1",
    'accept': "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
    'accept-encoding': "gzip, deflate, sdch",
    'accept-language': "zh-CN,zh;q=0.8,en-US;q=0.6,en;q=0.4",
    'cache-control': "no-cache",
}


def updateTable(sql, db):
    """执行update sql语句"""
    try:
        cur = db.cursor()
        cur.execute(sql)
        db.commit()
        cur.close()
    except Exception as e:
        logging.debug("Fail to update clue: ", type(Exception), str(e))


def get_json_from_url(title, url, proxy):
    retry = 0
    maxretry = 2
    jsonStr = ""
    while True:
        try:
            logging.debug('fetching %s %s' , title, unquote(url))
            response = requests.request("GET", url, headers=headers, timeout=10, proxies={"http": proxy})
            jsonStr = response.text
        except Exception as e:
            logging.debug('Exception: %s %s' % (e, unquote(url)))
            retry += 1
            if retry == maxretry:
                break
        else:
            logging.debug('fetched %s' % unquote(url))
            break

    return jsonStr


def saveResult(company):
    fields = list(company.keys())
    values = [company[field] for field in fields]
    try:
        fieldStr = ",".join([str(lookup[field]) for field in fields])
        values = [re.sub(r"'", r'"',str(value)) for value in values]
        valueStr = ",".join(["'" + str(value) + "'" for value in values])
        sql = """ INSERT INTO tianyancha_copy (%s) VALUES (%s);""" % (fieldStr, valueStr)
        logging.debug(sql)
    except Exception as e:
        logging.debug(str(e))
    with lock:
        logging.debug("get lock")
        updateTable(sql, dbwrite)


def parseSummary(title, cid, summary, proxy):
    company = {}
    summaryjson = json.loads(summary)["data"]

    for field in summaryjson:
        if field in lookup and int(summaryjson[field]) != 0:
            if field in idSet:
                url = INTERFACE[field] % cid
            elif field in nameSet:
                url = INTERFACE[field] % quote(title)
            else:
                logging.debug("字段不存在")
                continue

            string = get_json_from_url(title, url, proxy)
            try:
                company[field] = json.loads(string)
            except:
                pass
    company["cid"] = cid
    company["title"] = title
    company["summary"] = summaryjson

    #saveResult(company)
    saveToFile(cid, json.dumps(company, indent=2, ensure_ascii=False))


def saveToFile(company_id, content):
    logging.info("Saving %s" % (company_id))

    id = str(company_id)[0:4]

    out_dir = "./out/%s/" % id
    if not os.path.exists(out_dir):
        os.makedirs(out_dir)

    with open("%s/%s.json" % (out_dir, company_id), "w") as f:
        logging.debug(content[0:1000])
        f.write(content)


if __name__ == "__main__":
    titles = [
        "百灵时代（北京）文化传媒有限公司",
        "北京百替生物技术有限公司",
        "北京达文慧智投资中心（有限合伙）",
        "北京大道恒诚贸易有限公司",
        "北京鼎食汇餐饮管理有限公司蜀北漂分公司"
    ]
    cids = [
        "2318927539",
        "2358781630",
        "2348924653",
        "1095511796",
        "2312053324"
    ]
    summarys = [
        '{"state":"ok","message":"","special":"","vipMessage":"","isLogin":0,"data":{"changeCount":"2","punishment":"0","zhixing":"0","reportCount":"2","recruitCount":"0","checkCount":"0","dishonest":"0","jigouTzanli":"0","qualification":"0","staffCount":"2","companyPortray":"0","inverstCount":0,"patentCount":"0","bidCount":"0","tmCount":"1","companyRongzi":"0","taxCreditCount":"0","companyJingpin":"0","ownTaxCount":"0","branchCount":"0","equityUrl":"http://static.tianyancha.com/equity/788d8fb09274482c529dfab3b734ce5e.png","illegalCount":"0","holderCount":"2","icpCount":"1","lawsuitCount":"0","mortgageCount":"0","productinfo":"0","abnormalCount":"0","equityCount":"0","courtCount":"0","goudiCount":"0","bondCount":"0","cpoyRCount":"0","companyTeammember":"0","companyProduct":"0"}}',
        '{"state":"ok","message":"","special":"","vipMessage":"","isLogin":0,"data":{"changeCount":"2","punishment":"0","reportCount":"0","zhixing":"0","recruitCount":"0","checkCount":"0","dishonest":"0","jigouTzanli":"0","staffCount":"2","qualification":"0","companyPortray":"0","patentCount":"0","inverstCount":0,"bidCount":"0","tmCount":"0","companyRongzi":"0","taxCreditCount":"0","companyJingpin":"0","ownTaxCount":"0","branchCount":"0","equityUrl":"http://static.tianyancha.com/equity/a653fca7e1677a3b1ba91b9749fd5903.png","illegalCount":"0","holderCount":"1","icpCount":"0","lawsuitCount":"0","mortgageCount":"0","productinfo":"0","abnormalCount":"0","equityCount":"0","courtCount":"0","goudiCount":"0","cpoyRCount":"0","bondCount":"0","companyTeammember":"0","companyProduct":"0"}}',
        '{"state":"ok","message":"","special":"","vipMessage":"","isLogin":0,"data":{"changeCount":"2","punishment":"0","zhixing":"0","reportCount":"2","recruitCount":"0","checkCount":"0","dishonest":"0","jigouTzanli":"0","qualification":"0","staffCount":"0","companyPortray":"0","inverstCount":0,"patentCount":"0","bidCount":"0","tmCount":"0","companyRongzi":"0","taxCreditCount":"0","companyJingpin":"0","ownTaxCount":"0","branchCount":"0","equityUrl":"http://static.tianyancha.com/equity/125988709155a0944f1af7da6a09786d.png","illegalCount":"0","holderCount":"2","icpCount":"0","lawsuitCount":"0","mortgageCount":"0","productinfo":"0","abnormalCount":"0","equityCount":"0","courtCount":"0","goudiCount":"0","bondCount":"0","cpoyRCount":"0","companyTeammember":"0","companyProduct":"0"}}',
        '{"state":"ok","message":"","special":"","vipMessage":"","isLogin":0,"data":{"changeCount":"0","punishment":"0","zhixing":"0","reportCount":"1","recruitCount":"0","checkCount":"0","dishonest":"0","jigouTzanli":"0","qualification":"0","staffCount":"2","companyPortray":"0","inverstCount":0,"patentCount":"0","bidCount":"0","tmCount":"0","companyRongzi":"0","taxCreditCount":"0","companyJingpin":"0","ownTaxCount":"0","branchCount":"0","equityUrl":"http://static.tianyancha.com/equity/3f40653dd628eb1a82596f86f026e0fb.png","illegalCount":"0","holderCount":"2","icpCount":"0","lawsuitCount":"0","mortgageCount":"0","productinfo":"0","abnormalCount":"0","equityCount":"0","courtCount":"0","goudiCount":"0","bondCount":"0","cpoyRCount":"0","companyTeammember":"0","companyProduct":"0"}}',
        '{"state":"ok","message":"","special":"","vipMessage":"","isLogin":0,"data":{"changeCount":"0","punishment":"1","reportCount":"2","zhixing":"0","recruitCount":"0","checkCount":"1","dishonest":"0","jigouTzanli":"0","staffCount":"1","qualification":"0","companyPortray":"0","patentCount":"0","inverstCount":0,"bidCount":"0","tmCount":"0","companyRongzi":"0","taxCreditCount":"0","companyJingpin":"0","ownTaxCount":"0","branchCount":"0","equityUrl":"http://static.tianyancha.com/equity/184c08ab624b944caa7e7fbf639ed9e9.png","illegalCount":"0","holderCount":"0","icpCount":"0","lawsuitCount":"0","mortgageCount":"0","productinfo":"0","abnormalCount":"0","equityCount":"0","courtCount":"0","goudiCount":"0","cpoyRCount":"0","bondCount":"0","companyTeammember":"0","companyProduct":"0"}}'
    ]
    for i in range(5):
        parseSummary(titles[i], cids[i], summarys[i])
