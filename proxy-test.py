import json
import os
import time
from concurrent.futures import ThreadPoolExecutor

import requests

good = []


def proxy_test(proxy):
    headers = {
        'user-agent': "Mozilla/5.0 (iPhone; CPU iPhone OS 10_2_1 like Mac OS X) AppleWebKit/602.4.6 (KHTML, like Gecko) Version/10.0 Mobile/14D27 Safari/602.1",
        'accept': "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
        'accept-encoding': "gzip, deflate, sdch",
        'accept-language': "zh-CN,zh;q=0.8,en-US;q=0.6,en;q=0.4",
        'cache-control': "no-cache",
    }

    resp = requests.get(
        "http://m.tianyancha.com/expanse/findTeamMember.json?name=%E5%8C%97%E4%BA%AC%E7%99%BE%E5%BA%A6%E7%BD%91%E8%AE%AF%E7%A7%91%E6%8A%80%E6%9C%89%E9%99%90%E5%85%AC%E5%8F%B8&ps=5&pn=1",
        headers=headers, timeout=5,
        proxies={"http": proxy}
    )

    try:
        j = resp.json()
        print(resp.text)
        if j['state'] == 'ok':
            good.append(proxy)
            print("+++ Good:", proxy)
        else:
            print("   Bad ", proxy)
    except Exception as ex:
        print("   Bad ", proxy, ex)


while True:
    r = requests.get("http://s.zdaye.com/?api=201704181150565067&fitter=2&px=2").text
    new_proxies = r.splitlines(keepends=False)

    proxy_json = "valid-proxy.json"
    if os.path.exists(proxy_json):
        with open(proxy_json, "r") as f:
            new_proxies += json.loads(f.read())

    print("Num of proxies", len(new_proxies))

    executor = ThreadPoolExecutor(max_workers=1)

    for proxy in new_proxies:
        executor.submit(proxy_test, proxy)
    executor.shutdown()

    with open(proxy_json, "w") as f:
        f.write(json.dumps(good))

    time.sleep(60)
