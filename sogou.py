import datetime
import hashlib
import logging
import re
import time
import traceback
import urllib.parse
from concurrent.futures import ThreadPoolExecutor
from threading import RLock

import pymysql
import requests
import sys
from bs4 import BeautifulSoup
from requests.packages.urllib3.exceptions import InsecureRequestWarning

from modules.ProxyProvider import ProxyProvider, SequenceProxyProvider

requests.packages.urllib3.disable_warnings(InsecureRequestWarning)

logger = logging.getLogger()
logger.setLevel(logging.DEBUG)

ch = logging.StreamHandler()
formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s:')
ch.setFormatter(formatter)
logger.addHandler(ch)


class SogouCrawler():
    def __init__(self):
        self.cities = ["安徽", "福建", "广东", "甘肃", "广西", "贵州", "海南", "河北", "河南", "黑龙江", "湖北", "湖南", "吉林", "江苏", "江西", "辽宁",
                       "内蒙古", "宁夏", "青海", "四川", "山东", "陕西", "山西", "新疆", "西藏", "云南",
                       "浙江", "北京",
                       "重庆", "上海", "天津", "合肥", "芜湖", "蚌埠", "淮南", "马鞍山", "淮北", "铜陵", "安庆", "黄山", "滁州", "阜阳", "宿州", "六安",
                       "亳州", "池州", "宣城", "福州", "厦门", "莆田", "三明", "泉州", "漳州", "南平",
                       "龙岩", "宁德",
                       "东莞", "中山", "广州", "韶关", "深圳", "珠海", "汕头", "佛山", "江门", "湛江", "茂名", "肇庆", "惠州", "梅州", "汕尾", "河源",
                       "阳江", "清远", "潮州", "揭阳", "云浮", "嘉峪关", "兰州", "金昌", "白银", "天水",
                       "武威", "张掖",
                       "平凉", "酒泉", "庆阳", "定西", "陇南", "临夏回族自治州", "甘南藏族自治州", "南宁", "柳州", "桂林", "梧州", "北海", "防城港", "钦州",
                       "贵港", "玉林", "百色", "贺州", "河池", "来宾", "崇左", "贵阳", "六盘水", "遵义",
                       "安顺", "毕节",
                       "铜仁", "黔西南布依族苗族自治州", "黔东南州", "黔南布依族苗族自治州", "海口", "三亚", "省直辖县级行政区划", "石家庄", "唐山", "秦皇岛", "邯郸",
                       "邢台", "保定", "张家口", "承德", "沧州", "廊坊", "衡水", "郑州", "开封", "洛阳",
                       "平顶山", "安阳",
                       "鹤壁", "新乡", "焦作", "濮阳", "许昌", "漯河", "三门峡", "南阳", "商丘", "信阳", "周口", "驻马店", "省直辖县级行政区划", "哈尔滨",
                       "齐齐哈尔", "鸡西", "鹤岗", "双鸭山", "大庆", "伊春", "佳木斯", "七台河", "牡丹江",
                       "黑河", "绥化",
                       "大兴安岭地区", "武汉", "黄石", "十堰", "宜昌", "襄阳", "鄂州", "荆门", "孝感", "荆州", "黄冈", "咸宁", "随州", "恩施土家族苗族自治州",
                       "省直辖县级行政区划", "长沙", "株洲", "湘潭", "衡阳", "邵阳", "岳阳", "常德", "张家界",
                       "益阳", "郴州",
                       "永州", "怀化", "娄底", "湘西土家族苗族自治州", "长春", "吉林", "四平", "辽源", "通化", "白山", "松原", "白城", "延边朝鲜族自治州", "南京",
                       "无锡", "徐州", "常州", "苏州", "南通", "连云港", "淮安", "盐城", "扬州",
                       "镇江", "泰州", "宿迁",
                       "南昌", "景德镇", "萍乡", "九江", "新余", "鹰潭", "赣州", "吉安", "宜春", "抚州", "上饶", "沈阳", "大连", "鞍山", "抚顺", "本溪",
                       "丹东", "锦州", "营口", "阜新", "辽阳", "盘锦", "铁岭", "朝阳", "葫芦岛",
                       "呼和浩特", "包头",
                       "乌海", "赤峰", "通辽", "鄂尔多斯", "呼伦贝尔", "巴彦淖尔", "乌兰察布", "兴安盟", "锡林郭勒盟", "阿拉善盟", "银川", "石嘴山", "吴忠",
                       "固原", "中卫", "西宁", "海东", "海北藏族自治州", "黄南藏族自治州", "海南藏族自治州",
                       "果洛藏族自治州",
                       "玉树藏族自治州", "海西蒙古族藏族自治州", "成都", "自贡", "攀枝花", "泸州", "德阳", "绵阳", "广元", "遂宁", "内江", "乐山", "南充", "眉山",
                       "宜宾", "广安", "达州", "雅安", "巴中", "资阳", "阿坝藏族羌族自治州", "甘孜藏族自治州",
                       "凉山彝族自治州",
                       "济南", "青岛", "淄博", "枣庄", "东营", "烟台", "潍坊", "济宁", "泰安", "威海", "日照", "莱芜", "临沂", "德州", "聊城", "滨州",
                       "菏泽", "西安", "铜川", "宝鸡", "咸阳", "渭南", "延安", "汉中", "榆林", "安康",
                       "商洛", "太原",
                       "大同", "阳泉", "长治", "晋城", "朔州", "晋中", "运城", "忻州", "临汾", "吕梁", "乌鲁木齐", "克拉玛依", "吐鲁番地区", "哈密地区",
                       "昌吉回族自治州", "博尔塔拉蒙古自治州", "巴音郭楞蒙古自治州", "阿克苏地区", "克孜勒苏柯尔克孜自治州",
                       "喀什地区", "和田地区",
                       "伊犁哈萨克自治州", "塔城地区", "阿勒泰地区", "自治区直辖县级行政区划", "拉萨", "日喀则", "昌都地区", "山南地区", "那曲地区", "阿里地区", "林芝地区",
                       "昆明", "曲靖", "玉溪", "保山", "昭通", "丽江", "普洱", "临沧", "楚雄彝族自治州",
                       "红河哈尼族彝族自治州",
                       "文山壮族苗族自治州", "西双版纳傣族自治州", "大理白族自治州", "德宏傣族景颇族自治州", "怒江傈僳族自治州", "迪庆藏族自治州", "杭州", "宁波", "温州", "嘉兴",
                       "湖州", "绍兴", "金华", "衢州", "舟山", "台州", "丽水"]

        self.categories = ["燃气生产和供应业", "电力、热力生产和供应业", "水的生产和供应业", "建筑安装业", "土木工程建筑业", "房屋建筑业", "建筑装饰和其他建筑业", "批发业",
                           "零售业", "仓储业", "装卸搬运和运输代理业", "管道运输业", "航空运输业", "水上运输业", "邮政业",
                           "铁路运输业",
                           "道路运输业",
                           "渔业", "农、林、牧、渔服务业", "农业", "林业", "畜牧业", "开采辅助活动", "其他采矿业", "黑色金属矿采选业", "有色金属矿采选业", "煤炭开采和洗选业",
                           "石油和天然气开采业", "非金属矿采选业", "专用设备制造业", "汽车制造业", "金属制品业",
                           "通用设备制造业",
                           "计算机、通信和其他电子设备制造业", "铁路、船舶、航空航天和其他运输设备制造业", "电气机械和器材制造业", "金属制品、机械和设备修理业", "废弃资源综合利用业",
                           "其他制造业", "仪器仪表制造业", "造纸和纸制品业", "印刷和记录媒介复制业", "文教、工美、体育和娱乐用品制造业",
                           "石油加工、炼焦和核燃料加工业", "化学原料和化学制品制造业", "医药制造业", "化学纤维制造业", "橡胶和塑料制品业", "非金属矿物制品业", "有色金属冶炼和压延加工业",
                           "黑色金属冶炼和压延加工业", "皮革、毛皮、羽毛及其制品和制鞋业", "纺织业", "纺织服装、服饰业",
                           "酒、饮料和精制茶制造业",
                           "烟草制品业", "农副食品加工业", "食品制造业", "家具制造业", "木材加工和木、竹、藤、棕、草制品业", "租赁业", "商务服务业", "研究和试验发展",
                           "专业技术服务业", "科技推广和应用服务业", "公共设施管理业", "生态保护和环境治理业", "水利管理业", "居民服务业",
                           "机动车、电子产品和日用产品修理业", "其他服务业", "餐饮业", "住宿业", "互联网和相关服务", "软件和信息技术服务业", "电信、广播电视和卫星传输服务",
                           "资本市场服务", "货币金融服务", "其他金融业", "保险业", "房地产业", "国际组织", "卫生", "社会工作",
                           "教育",
                           "基层群众自治组织", "群众团体、社会团体和其他成员组织", "社会保障", "人民政协、民主党派", "国家机构", "中国共产党机关", "体育", "娱乐业",
                           "广播、电视、电影和影视录音制作业", "文化艺术业", "新闻和出版业"]

        self.match_url = re.compile('http[s]?://www.tianyancha.com/company/\d+')
        self.match_company_name = re.compile('(.*?)_【', flags=re.UNICODE)
        self.headers = {
            "Accept-Encoding": "gzip, deflate, br",
            "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8",
            "Cache-Control": "no-cache",
            'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.90 Safari/537.36'
        }

        self.connection = pymysql.connect(host='rds0710650me01y6d3ogo.mysql.rds.aliyuncs.com',
                                          user='yunkedata',
                                          password='XingDongJia@2016',
                                          db='dataprocess',
                                          charset='utf8',
                                          cursorclass=pymysql.cursors.DictCursor)
        self.lock = RLock()
        self.proxy_provider = SequenceProxyProvider(num_of_proxies=400)
        self.start_time = datetime.datetime.now()
        self.total = 0

    def get_auth_token(self):
        my_app_key = "111877882"
        app_secret = "1451452bef315cb2da8b09ef0cba221a"
        mayi_url = 's4.proxy.mayidaili.com'
        mayi_port = '8123'
        mayi_proxy = {'https': 'https://{}:{}'.format(mayi_url, mayi_port)}

        timesp = '{}'.format(time.strftime("%Y-%m-%d %H:%M:%S"))
        codes = app_secret + 'app_key' + my_app_key + 'timestamp' + timesp + app_secret
        sign = hashlib.md5(codes.encode('utf-8')).hexdigest().upper()

        authHeader = 'MYH-AUTH-MD5 sign=' + sign + '&app_key=' + my_app_key + '&timestamp=' + timesp

        s = requests.Session()
        s.headers.update({
            'user-agent': "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36"})
        s.headers.update({'Proxy-Authorization': authHeader})
        s.proxies.update(mayi_proxy)

        return s

    def fetch(self, job):
        city, category, i = job
        logging.debug(job)
        url = 'http://www.sogou.com/web?query=%s+"%s"+%d+site%%3Atianyancha.com&ie=utf8' % (city, category, i)

        retry_count = 0
        while retry_count < 10:
            retry_count += 1
            proxy = self.proxy_provider.pick()
            try:
                # token = self.get_auth_token()
                resp = requests.get(url, timeout=15, headers=self.headers, verify=False, allow_redirects=False,
                                    proxies={"http": proxy.url})

                if '搜狗搜索' not in resp.text and category not in resp.text:
                    proxy.parse_error()
                    logging.debug("Not valid content %s", resp.text[0:200])
                    continue

                soup = BeautifulSoup(resp.text, 'lxml')

                names = []
                links = []
                for item in soup.findAll('a', {"href": re.compile('www.tianyancha.com/company/*?')}):
                    if item.has_attr('class'):
                        continue
                    name_found = self.match_company_name.findall(item.text.strip())
                    if len(name_found) != 0:
                        names.append(name_found[0])
                        links.append(item['href'])

                logging.debug("%s, %s, %s", url, links, names)
                if len(links) != len(names):
                    logging.debug("links and names are not valid", links, names)
                    proxy.parse_error()
                    continue

                proxy.success()
                self.proxy_provider.dump()
                with self.lock:
                    logging.debug("Found valid names %s, %s", names, links)
                    for i in range(0, len(names)):
                        with self.connection.cursor() as cursor:
                            sql = """INSERT IGNORE company_names_sogou (name, URL) VALUES ('%s','%s')""" % (
                                names[i], links[i])
                            cursor.execute(sql)
                            logging.debug(sql)
                    self.connection.commit()
                    self.total += len(names)

                timespent = datetime.datetime.now() - self.start_time
                speed = self.total / timespent.total_seconds() * 60
                logging.debug("Speed %0.2f", speed * 100)

                next = soup.find("a", {"id": "sogou_next"})
                if next is None:
                    break

                url = 'http://www.sogou.com/web' + next['href']
            except Exception as ex:
                proxy.connection_error()
                logging.exception(ex)

        self.mark_searched(city, category, i)

    def mark_searched(self, city, category, i):
        with self.lock:
            with self.connection.cursor() as cursor:
                sql = """INSERT IGNORE sogou_searched (city, category, i) VALUES('%s','%s', '%s')""" % (
                city, category, i)
                cursor.execute(sql)
            self.connection.commit()

    def is_searched(self, city, category, i):
        with self.lock:
            with self.connection.cursor() as cursor:
                # Read a single record
                sql = "SELECT * FROM sogou_searched WHERE city='%s' AND category='%s' AND i='%s'" % (city, category, i)
                cursor.execute(sql)
                result = cursor.fetchone()
                return result is not None

    def start(self):
        for i in range(0, 5000, 99):
            executor = ThreadPoolExecutor(max_workers=200)
            for city in self.cities:
                for category in self.categories:
                    if self.is_searched(city, category, i):
                        continue

                    executor.submit(self.fetch, (city, category, i))

            executor.shutdown(wait=True)


SogouCrawler().start()
