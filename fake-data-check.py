import glob
import os
import json

dirs = glob.glob("./out/*")

for i in range(0, len(dirs) - 1):
    dir_a = dirs[i]
    dir_b = dirs[i + 1]
    companies = glob.glob(dir_a + "/*")
    for company in sorted(companies):
        project_id = company.split("/")[-1]

        if not os.path.isdir(os.path.join(dir_b, project_id)):
            print("skip " + project_id)
            continue

        info_a = os.path.join(dir_a, project_id, "company_info.json")
        info_b = os.path.join(dir_b, project_id, "company_info.json")

        j_a = json.loads(open(info_a, encoding="utf-8").read())
        j_b = json.loads(open(info_b, encoding="utf-8").read())

        if j_a['data']['regLocation'] != j_b['data']['regLocation']:
            print("Fake data", info_a, info_b)
            raise Exception
        else:
            print("OK")
